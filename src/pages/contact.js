import Head from "next/head"

export default function Contact() {
    return (
        <>
            <Head>
                <title>Contact - ARR Global Services LLC</title>
            </Head>
            <div className="flex justify-center h-full py-10">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="bg-white md:shadow-md mb-4 border md:border-0">
                        <div className="bg-gray-100 px-4 py-4 text-center md:rounded-t">
                            <h1 className="font-semibold">Contact Us</h1>
                        </div>

                        <div className="container px-5 py-10 mx-auto flex sm:flex-nowrap flex-wrap">
                            <div className="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d57728.74359037531!2d55.290583950160496!3d25.269022018743552!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f434f37cdae93%3A0xde756363a1b78491!2sDeira%20-%20Dubai%20-%20United%20Arab%20Emirates!5e0!3m2!1sen!2smy!4v1619438306720!5m2!1sen!2smy" width="100%" height="100%" className="absolute inset-0" frameborder="0" title="map" marginheight="0" marginwidth="0" scrolling="no" allowfullscreen="" loading="lazy"></iframe>
                                <div className="bg-white relative flex flex-wrap py-6 rounded shadow-md">
                                    <div className="lg:w-1/2 px-6">
                                        <h2 className="title-font font-semibold text-gray-900 tracking-widest text-xs">ADDRESS</h2>
                                        <p className="leading-normal my-2">ARR GLOBAL ELECTROMECHANICAL SERVICES LLC</p>
                                        <p className="leading-normal my-3">Po Box  : 232571 - Dubai - United Arab Emirates</p>
                                        <h2 className="title-font font-semibold text-gray-900 tracking-widest text-xs my-2">WORKING HOURS</h2>
                                        <p className="leading-relaxed">Sat - Thu: 8:00 AM - 6:00 PM</p>
                                    </div>
                                    <div className="lg:w-1/2 px-6 mt-4 lg:mt-0">
                                        <h2 className="title-font font-semibold text-gray-900 tracking-widest text-xs">EMAIL</h2>
                                        <a className="text-indigo-500 leading-relaxed">info@arrglobalservice.com</a>
                                        <h2 className="title-font font-semibold text-gray-900 tracking-widest text-xs mt-4">PHONE</h2>
                                        <p className="leading-relaxed">+971-042344161</p>
                                    </div>
                                </div>
                            </div>
                            <div className="lg:w-1/3 md:w-1/2 bg-white flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
                                <h2 className="text-gray-900 text-lg mb-1 font-medium title-font">Get in touch</h2>
                                <p className="leading-relaxed mb-5 text-gray-600">Feel free to ask any questions over the phone, or get in touch via our contact form below.</p>
                                <div className="relative mb-4">
                                    <label for="name" className="leading-7 text-sm text-gray-600">Name</label>
                                    <input type="text" id="name" name="name" className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                                </div>
                                <div className="relative mb-4">
                                    <label for="email" className="leading-7 text-sm text-gray-600">Email</label>
                                    <input type="email" id="email" name="email" className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                                </div>
                                <div className="relative mb-4">
                                    <label for="message" className="leading-7 text-sm text-gray-600">Message</label>
                                    <textarea id="message" name="message" className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
                                </div>
                                <button className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Submit</button>
                                <p className="text-xs text-gray-500 mt-3">Chicharrones blog helvetica normcore iceland tousled brook viral artisan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}