// import Router from 'next/router'
import SiteLayout from '../components/Layouts/SiteLayout'
import '../styles/globals.css'
import 'swiper/swiper.scss'

function MyApp({ Component, pageProps }) {
  // const Layout = Component.Layout || DefaultLayout;
  return (
    <SiteLayout>
      <Component {...pageProps} />
    </SiteLayout>
  )
}

export default MyApp
