import Head from "next/head"

export default function Projects() {
    const projects = [
        { 'name': 'Jumeirah Golf Estate', 'area': 'JGE, World Flower', 'details': 'Ms, SANAZ  AUNTONY , Villa No: 32/33', 'status': 'Completed', 'image': 'https://www.wsp.com/-/media/Service/Middle-East/bnrmeMechanical-Electrical-and-Plumbing.jpg' },
        { 'name': 'Palm Jumeirah D-97', 'area': 'Palm Jumeirah, D-97', 'details': 'Dr,Leena , Villa  no : D-97', 'status': 'Progress', 'image': '/images/works/Palm-Jumeirah-VillaNo-D-97/3.jpg' },
        { 'name': 'Damac Exclusive Tower', 'area': 'T-come, Al Barsha-2', 'details': 'DAMAC  Exclusive Tower', 'status': 'Completed', 'image': 'https://service.works/images/plumbing-electrical-service-mobile.png' },
        { 'name': 'ECC Renovation Head Office', 'area': 'Al Qouz ind-3', 'details': 'Engineering  Contracting  Company', 'status': 'Progress', 'image': '/images/works/Jebel-Ali -Ware House-Plumbing-work/4.jpg' },
        { 'name': 'Dwight School  Dubai', 'area': 'Al Barsha south', 'details': 'Dwight School', 'status': 'Progress', 'image': 'https://www.wsp.com/-/media/Service/Middle-East/bnrmeMechanical-Electrical-and-Plumbing.jpg' },
        { 'name': 'Brighton College Dubai', 'area': 'Al Barsha south', 'details': 'Brighton College Dubai', 'status': 'Completed', 'image': '/images/works/Grand-Belle-VUE Hotel-Electrical-work/4.jpg' },
        { 'name': 'Dubai, Hills Villa :47', 'area': 'Dubai, Hills.Fireway villa : 47', 'details': 'Green Dream  Landscaping', 'status': 'Progress', 'image': 'https://service.works/images/plumbing-electrical-service-mobile.png' },
        { 'name': 'Dubai, Hills Villa :360', 'area': 'Dubai, Hills.Sidra-2 villa : 360', 'details': 'Green Dream  Landscaping', 'status': 'Completed', 'image': 'https://www.wsp.com/-/media/Service/Middle-East/bnrmeMechanical-Electrical-and-Plumbing.jpg' },
        { 'name': 'Grand Belle Vue', 'area': 'T-come, Al Barsha-2', 'details': 'Grand Belle Vue  Hotel', 'status': 'Progress', 'image': '/images/works/Grand-Belle-VUE Hotel-Electrical-work/4.jpg' },
        { 'name': 'Jebel Ali  warehouse', 'area': 'Jebel Ali  Ind-1', 'details': 'Ms, Latifa Juma Nasser', 'status': 'Completed', 'image': '/images/works/Jebel-Ali -Ware House-Plumbing-work/4.jpg' },
        { 'name': 'AYYOOR  Contracting', 'area': 'Al Sofwa Villa', 'details': 'Dr, Adbul Rahim Ahmed', 'status': 'Completed', 'image': 'https://service.works/images/plumbing-electrical-service-mobile.png' },
    ];

    return (
        <>
            <Head>
                <title>Projects - ARR Global Services LLC</title>
            </Head>
            <div className="flex justify-center h-full py-10">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="bg-white md:shadow-md mb-4 border md:border-0">
                        <div className="bg-gray-100 px-4 py-4 text-center md:rounded-t">
                            <h1 className="font-semibold">Our Projects</h1>
                        </div>

                        <div className="w-full h-full md:h-auto grid grid-cols-1 md:grid-cols-3 gap-4 px-2 py-4">
                            {projects.map((project) => (
                                <div key={project} className="select-none mb-6 w-full">
                                    <div className="relative pb-64">
                                        <a href="#" className="cursor-pointer">
                                            <img className="absolute w-full h-full rounded-lg object-cover border-b shadow-md cursor-pointer" src={project.image} alt={project.name} />
                                        </a>
                                    </div>
                                    <div className="px-4 -mt-16 relative">
                                        <div className="bg-white rounded-lg shadow-lg border">
                                            <div className="p-5">
                                                <div className="flex justify-between items-center">
                                                    <div className="opacity-75 text-xs">
                                                        <a className="hover:underline" href="#">{project.area}</a>
                                                    </div>

                                                    <span className={`${project.status == 'Progress' ? 'text-green-700 bg-green-200' : 'text-blue-900 bg-blue-200'} rounded-full px-3 py-1 text-xs select-none`}>{project.status}</span>
                                                </div>

                                                <a className="text-gray-800 block mt-2 truncate hover:underline font-medium text-lg" href="#">{project.name} - {project.area}</a>
                                            </div>

                                            <div className="flex justify-between items-center pb-3 px-4">
                                                <div>
                                                    <div className="text-gray-800 text-lg">
                                                        {/* <span className="font-medium">$249</span> */}
                                                    </div>
                                                </div>

                                                <div>
                                                    <a className="btn-link flex items-center text-xs text-indigo-600 hover:underline" href="#">
                                                        View More &rarr;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>))}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}