import Head from "next/head"

export default function About() {
    return (
        <>
            <Head>
                <title>About - ARR Global Services LLC</title>
            </Head>
            <div className="flex justify-center h-full py-10">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="bg-white md:shadow-md mb-4 border md:border-0">
                        <div className="bg-gray-100 px-4 py-4 text-center md:rounded-t">
                            <h1 className="font-semibold">About Us</h1>
                        </div>

                        <div className="container">
                            <div className="flex flex-col lg:flex-row justify-between items-center p-6">
                                <div className="sm:w-5/6 md:w-9/10 lg:w-3/5 text-center md:text-left">
                                    <h2 className="font-header text-black text-2xl">Our History</h2>
                                    <p className="font-body font-light text-sm text-black pt-5 pb-4 leading-loose">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed enim sem, consectetur ut posuere quis, cursus quis sapien. Pellentesque lacinia erat id dignissim eleifend. Integer eget massa in urna ultrices lacinia. Aliquam sodales sit amet sapien ut vehicula. Donec lacinia augue vulputate nulla porttitor pellentesque. Nunc volutpat porta tincidunt. Pellentesque lacinia erat id dignissim eleifend. <br /> <br /> Donec sodales ultricies justo, a luctus lorem gravida ut. Duis et ultrices leo. Nulla dignissim eleifend dolor id ultrices. Integer tempor vulputate pellentesque. Etiam tincidunt urna at tortor tincidunt ultrices. In dictum molestie dignissim. Fusce vehicula, ligula at commodo fringilla, elit turpis pulvinar elit, vitae hendrerit nisi urna non justo. Nam arcu enim, dignissim congue nisl ac, luctus molestie lacus.</p>
                                </div>
                                <div className="lg:ml-10 xl:ml-20 lg:mr-auto w-full sm:w-5/6 md:w-9/10 lg:w-2/5 h-88 sm:h-130 md:h-156 lg:h-124 xl:h-136 mt-6 lg:mt-0">
                                    <img src="/images/arr-full.png" className="w-96" />
                                </div>
                            </div>

                            <div className="sm:w-5/6 md:w-9/10 lg:w-full mx-auto lg:mx-0 text-center md:text-left p-6">
                                <h2 className="font-header text-black text-2xl">Company Values</h2>
                                <p className="font-body font-light text-sm text-black pt-5 pb-4 leading-loose">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed enim sem, consectetur ut posuere quis, cursus quis sapie. Pellentesque lacinia erat id dignissim eleifend. Integer eget massa in urna ultrices lacinia. Aliquam sodales sit amet sapien utvula. Donec laciia augue vulputate nulla porttitor pellentesque. Nunc volutpat porta tincidunt. Fusce vehicula, ligula at commodo fringilla. Donec sodales ultricies justo, a luctus lorem ravida ut. Duis et ultrices leo.</p>

                                <div className="flex flex-wrap justify-between pt-10 lg:-mx-8 xl:-mx-10">

                                    <div className="lg:w-1/2 flex flex-col md:flex-row pb-12 lg:px-8 xl:px-10">
                                        <div className="w-1/6 mx-auto md:mx-0">
                                            <svg className="w-20 h-auto text-green-700" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
                                        </div>
                                        <div className="md:w-5/6 pt-5 md:pt-0 lg:pl-4 xl:pl-0">
                                            <p className="font-body font-semi-bold text-primary text-base md:text-lg uppercase">INTEGRITY</p>
                                            <p className="font-body font-light text-black text-sm pt-2 leading-loose">Lorem ipsum dolor sit amet, consectetur dipising elit. Sednim sem, consectetur ut posuere quis, cursus quis sapie. Peletese lacinia dolor sit amet.</p>
                                        </div>
                                    </div>

                                    <div className="lg:w-1/2 flex flex-col md:flex-row pb-12 lg:px-8 xl:px-10">
                                        <div className="w-1/6 mx-auto md:mx-0">
                                            <svg className="w-20 h-auto text-green-700" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
                                        </div>
                                        <div className="md:w-5/6 pt-5 md:pt-0 lg:pl-4 xl:pl-0">
                                            <p className="font-body font-semi-bold text-primary text-base md:text-lg uppercase">Collaboration</p>
                                            <p className="font-body font-light text-black text-sm pt-2 leading-loose">Lorem ipsum dolor sit amet, consectetur dipising elit. Sednim sem, consectetur ut posuere quis, cursus quis sapie. Peletese lacinia dolor sit amet.</p>
                                        </div>
                                    </div>

                                    <div className="lg:w-1/2 flex flex-col md:flex-row pb-12 lg:px-8 xl:px-10">
                                        <div className="w-1/6 mx-auto md:mx-0">
                                            <svg className="w-20 h-auto text-green-700" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
                                        </div>
                                        <div className="md:w-5/6 pt-5 md:pt-0 lg:pl-4 xl:pl-0">
                                            <p className="font-body font-semi-bold text-primary text-base md:text-lg uppercase">Client Focus</p>
                                            <p className="font-body font-light text-black text-sm pt-2 leading-loose">Lorem ipsum dolor sit amet, consectetur dipising elit. Sednim sem, consectetur ut posuere quis, cursus quis sapie. Peletese lacinia dolor sit amet.</p>
                                        </div>
                                    </div>

                                    <div className="lg:w-1/2 flex flex-col md:flex-row pb-12 lg:px-8 xl:px-10">
                                        <div className="w-1/6 mx-auto md:mx-0">
                                            <svg className="w-20 h-auto text-green-700" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
                                        </div>
                                        <div className="md:w-5/6 pt-5 md:pt-0 lg:pl-4 xl:pl-0">
                                            <p className="font-body font-semi-bold text-primary text-base md:text-lg uppercase">Service</p>
                                            <p className="font-body font-light text-black text-sm pt-2 leading-loose">Lorem ipsum dolor sit amet, consectetur dipising elit. Sednim sem, consectetur ut posuere quis, cursus quis sapie. Peletese lacinia dolor sit amet.</p>
                                        </div>
                                    </div>

                                    <div className="lg:w-1/2 flex flex-col md:flex-row pb-12 lg:px-8 xl:px-10">
                                        <div className="w-1/6 mx-auto md:mx-0">
                                            <svg className="w-20 h-auto text-green-700" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
                                        </div>
                                        <div className="md:w-5/6 pt-5 md:pt-0 lg:pl-4 xl:pl-0">
                                            <p className="font-body font-semi-bold text-primary text-base md:text-lg uppercase">INTELLECTUAL RIGOR</p>
                                            <p className="font-body font-light text-black text-sm pt-2 leading-loose">Lorem ipsum dolor sit amet, consectetur dipising elit. Sednim sem, consectetur ut posuere quis, cursus quis sapie. Peletese lacinia dolor sit amet.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}