import Link from 'next/link'
import { useRouter } from "next/router";

export default function Header() {
    const router = useRouter();

    return (
        <div className="bg-gray-100 w-full z-20 top-0">
            {/* <div className="container mx-auto max-w-6xl m-auto text-gray-800"> */}
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex justify-between items-center py-8">
                    <Link href="/">
                        <a>
                            <img src="/images/arr.png" className="w-36 h-auto" alt="logo" />
                        </a>
                    </Link>
                    <ul className="flex items-center">

                        <li className="px-4 xl:px-6 hidden lg:block">
                            <Link href="/">
                                <a className={`text-md font-semibold uppercase transition-colors ${router.pathname == "/" ? "text-white bg-blue-900 rounded-md px-2 py-2" : "hover:text-pink-700 text-gray-700"}`}>Home</a>
                            </Link>
                        </li>

                        <li className="px-4 xl:px-6 hidden lg:block">
                            <Link href="/about">
                                <a className={`text-md font-semibold uppercase transition-colors ${router.pathname == "/about" ? "text-white bg-blue-900 rounded-md px-2 py-2" : "hover:text-pink-700 text-gray-700"}`}>About us</a>
                            </Link>
                        </li>

                        <li className="px-4 xl:px-6 hidden lg:block">
                            <Link href="/services">
                                <a className={`text-md font-semibold uppercase transition-colors ${router.pathname == "/services" ? "text-white bg-blue-900 rounded-md px-2 py-2" : "hover:text-pink-700 text-gray-700"}`}>Services</a>
                            </Link>
                        </li>

                        <li className="px-4 xl:px-6 hidden lg:block">
                            <Link href="/projects">
                                <a className={`text-md font-semibold uppercase transition-colors ${router.pathname == "/projects" ? "text-white bg-blue-900 rounded-md px-2 py-2" : "hover:text-pink-700 text-gray-700"}`}>Projects</a>
                            </Link>
                        </li>

                        <li className="px-4 xl:px-6 hidden lg:block">
                            <Link href="/contact">
                                <a className={`text-md font-semibold uppercase transition-colors ${router.pathname == "/contact" ? "text-white bg-blue-900 rounded-md px-2 py-2" : "hover:text-pink-700 text-gray-700"}`}>Contact us</a>
                            </Link>
                        </li>

                    </ul>
                    <Link href="tel:971553979456">
                        <a className="lg:block text-white uppercase btn bg-blue-900 px-3 py-3 rounded-md">Call us</a>
                    </Link>
                    <i className="bx bx-menu text-white text-4xl block lg:hidden"></i>
                </div>
            </div>
        </div>
    )
}
